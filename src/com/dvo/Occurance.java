package com.dvo;

/**
 * Created by Danyl on 004 04.08.17.
 */
public class Occurance {
    private String item;
    private int occurances;
    public Occurance(String item, int occurances){
        this.item = item;
        this.occurances = occurances;
    }

    public int getOccurances() {
        return occurances;
    }

    public String getItem() {
        return item;
    }
}
