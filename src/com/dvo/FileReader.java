package com.dvo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Danyl on 004 04.08.17.
 */
public class FileReader {

    public String readFileToString(String path) throws IOException {
        return new String((Files.readAllBytes(Paths.get(path)))).replaceAll("\\.|,","");
    }
}
