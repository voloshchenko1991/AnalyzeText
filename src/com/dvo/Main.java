package com.dvo;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        Analyzer analyzer = new Analyzer();
        List<Occurance> listOfOccurances ;
        List<String> splittedList;
        List<String> sortedList;
        List<String> listWithoutShortWords;
        FileReader fileReader = new FileReader();
        String inputFromFile;
        inputFromFile = fileReader.readFileToString("F:/test.txt");
        splittedList = analyzer.splitStringToCollection(inputFromFile);
        listOfOccurances = analyzer.foundMultipleOccurances(splittedList);
        sortedList = analyzer.sortWordsByAlphabet(splittedList);
        System.out.println("-----------------------------------------");
        System.out.println("Checking for multiple occurance:\n");
        listOfOccurances.forEach((a)->
                System.out.println("Item \"" + a.getItem() + "\" occures " + a.getOccurances() + " times\n"));
        System.out.println("-----------------------------------------");
        System.out.println("Alphabetically sorted list: \n");
        sortedList.forEach((a)-> System.out.println(a));
        System.out.println("-----------------------------------------");
        listWithoutShortWords = analyzer.removeShortWords(sortedList);
        System.out.println("List without short words: \n");
        listWithoutShortWords.forEach((a)-> System.out.println(a));
        System.out.println("-----------------------------------------");
    }
}
