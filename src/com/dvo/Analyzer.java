package com.dvo;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Danyl on 004 04.08.17.
 */
public class Analyzer {
    private String forFileInput;

    public List<String> splitStringToCollection(String inputString){
        List<String> myList = new ArrayList<>();
        for (String val:inputString.split(" ")) {
            myList.add(val);
        }
        return myList;
    }

    public List<Occurance> foundMultipleOccurances(List<String> inputList){
        List<Occurance> result = new ArrayList<>();
        String antiDoubleFrequencyChecker = "";
        for (int i = 0; i < inputList.size(); i++) {
            if((Collections.frequency(inputList,inputList.get(i)) > 1)&&
                    (!antiDoubleFrequencyChecker.contains(inputList.get(i)))){
                antiDoubleFrequencyChecker += inputList.get(i);
                result.add(new Occurance(inputList.get(i),Collections.frequency(inputList,inputList.get(i))));
            }
        }
        return result;
    }

    public List<String> sortWordsByAlphabet(List<String> inputList){
        return inputList.stream().sorted().collect(Collectors.toList());
    }

    public List<String> removeShortWords(List<String> inputList){
        List<String> result = new ArrayList<>();
        for (int i = 0; i < inputList.size(); i++) {
            if(inputList.get(i).length() > 3){
                result.add(inputList.get(i));
            }
        }
        return result;
    }





}
