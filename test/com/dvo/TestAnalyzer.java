package com.dvo;

import com.dvo.Analyzer;
import com.dvo.FileReader;
import com.dvo.Occurance;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Created by Danyl on 004 04.08.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TestAnalyzer {

    @Mock
    FileReader fileReader;

    @InjectMocks
    Analyzer analyzer;

    List<String> testList = new ArrayList<>();
    String testString = "Danyl Oleg Mihail Alexandr Danyl aa";
    @Before
    public void before(){
        testList.add("Danyl");
        testList.add("Oleg");
        testList.add("Mihail");
        testList.add("Alexandr");
        testList.add("Danyl");
        testList.add("aa");
    }

    @Test
    public void testForNumberOfInvocationsOfReadFile() throws IOException {
        when(fileReader.readFileToString(anyString())).thenReturn("Danyl Oleg Mihail Alexandr Danyl aa");
        String testInputString;
        testInputString = fileReader.readFileToString("F:/test.txt");
        analyzer.splitStringToCollection(testInputString);
        verify(fileReader, times(1)).readFileToString(anyString());
    }

    @Test
    public void testSplit() throws IOException {
        Assert.assertEquals(testList,analyzer.splitStringToCollection(testString));
    }

    @Test
    public void testOccurance(){
        List<Occurance> testOccur;
        testOccur = analyzer.foundMultipleOccurances(testList);
        Assert.assertTrue(testOccur.get(0).getItem() == "Danyl" &&
        testOccur.get(0).getOccurances() == 2);
    }

    @Test
    public void testSort(){
        List<String> sortedList = new ArrayList<>();
        sortedList.add("Alexandr");
        sortedList.add("Danyl");
        sortedList.add("Danyl");
        sortedList.add("Mihail");
        sortedList.add("Oleg");
        sortedList.add("aa");
        Assert.assertEquals(sortedList, analyzer.sortWordsByAlphabet(testList));
    }

    @Test
    public void testRemove(){
        List<String> optimizedList = new ArrayList<>();
        optimizedList.add("Danyl");
        optimizedList.add("Oleg");
        optimizedList.add("Mihail");
        optimizedList.add("Alexandr");
        optimizedList.add("Danyl");
        Assert.assertEquals(optimizedList, analyzer.removeShortWords(testList));
    }





}
